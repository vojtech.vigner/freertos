# FreeRTOS
This is reduced version of FreeRTOS library for my personal projects. It contains support only for GCC and STM microcontrollers.

Original library is on [GitHub](https://github.com/freertos/freertos). More information can be found here [https://www.freertos.org/index.html](https://www.freertos.org/index.html) 

This library is compatible with [PlatformIO Library Management System](https://docs.platformio.org/en/latest/librarymanager/creating.html).

For use with platformio you can add library in platformio.ini like this:
>lib_deps = 
>
>&nbsp;&nbsp;&nbsp;&nbsp;freertos = https://gitlab.com/vojtech.vigner/freertos.git

In order to specify build flags:

+ FREERTOS_DEV which is relative path to port files for specific MCU core from portable/GCC folder
+ FREERTOS_MEM which is heap implementation (1 - 5)

>build_flags = 
>
>&nbsp;&nbsp;&nbsp;&nbsp;-D FREERTOS_DEV=ARM_CM7/r0p1
>
>&nbsp;&nbsp;&nbsp;&nbsp;-D FREERTOS_MEM=4

# Original FreeRTOS readme.txt
## Directories:

+ The **FreeRTOS/Source** directory contains the FreeRTOS source code, and contains
  its own readme file.

+ The **FreeRTOS/Demo** directory contains a demo application for every official
FreeRTOS port, and contains its own readme file.

+ The **FreeRTOS/Test** directory contains the tests performed on common code and the portable layer code, and contains its own readme file.

See [FreeRTOS/SourceOrganization](http://www.freertos.org/a00017.html) for full details of the directory structure and information on locating the files you require.

The easiest way to use FreeRTOS is to start with one of the pre-configured demo 
application projects (found in the FreeRTOS/Demo directory).  That way you will
have the correct FreeRTOS source files included, and the correct include paths
configured.  
Once a demo application is building and executing you can remove
the demo application file, and start to add in your own application source
files.

### See also -
+ [Quick Start Guide](http://www.freertos.org/FreeRTOS-quick-start-guide.html)

+ [FAQ](http://www.freertos.org/FAQHelp.html)



    
