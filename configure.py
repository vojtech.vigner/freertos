########################## Helper functions ##########################
from os.path import join, realpath, exists
log_prefix = "FreeRTOS: "


def prError(skk):
    print("\033[91mError: {}{}\033[00m" .format(log_prefix, skk))
    exit(1)


def prWarning(skk):
    print("\033[93m{}{}\033[00m" .format(log_prefix, skk))


def prMessage(skk):
    print("\033[92m{}{}\033[00m" .format(log_prefix, skk))


def doAddFilter(new_path):
    new_path = join("..", new_path)
    prMessage("Adding path to src filter: " + new_path)
    env.Append(SRC_FILTER=["+<" + new_path + ">"])


def doAddHeader(new_path):
    new_path = realpath(new_path)
    prMessage("Adding header folder: " + new_path)
    env.Append(CPPPATH=[new_path])


def chPath(new_path):
    if not exists(new_path):
        prError("Path not exists: " + new_path)
########################## Helper functions ##########################


Import('env')

env.Replace(SRC_FILTER=["+<*>"])

param_dev = "FREERTOS_DEV"
param_buf = "FREERTOS_MEM"

# Device configuration
new_path = ""
for item in env.get("CPPDEFINES", []):
    if isinstance(item, tuple) and item[0] == param_dev:
        # Add header folder
        new_path = join("portable/GCC", item[1])
        chPath(new_path)
        doAddHeader(new_path)

        # Add source to filter
        new_path = join("portable/GCC", item[1])
        chPath(new_path)
        doAddFilter(join(new_path, "*"))
        break

# If parameter not defined exit
if new_path == "":
    prError("'" + param_dev + "' not defined!!! eg. build_flags = -D " +
            param_dev + "=ARM_CM7/r0p1")

# Memory configuration
new_path = ""
for item in env.get("CPPDEFINES", []):
    if isinstance(item, tuple) and item[0] == param_buf:
        # Add source to filter
        new_path = join("portable/MemMang/", "heap_" + str(item[1]) + ".c")
        chPath(new_path)
        doAddFilter(new_path)
        break

# If parameter not defined default to heap_4.c
if new_path == "":
    prWarning("'" + param_buf + "' not defined!!! Defaulting to heap_4")
    doAddFilter("portable/MemMang/heap_4.c")
